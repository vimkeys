var evalPromptBuffer = "";

// this is mainly for debugging
function evalPrompt() {
	var typed = prompt( "javascript console:", evalPromptBuffer );

	if ( typed == null )
		return;

	evalPromptBuffer = typed;

	try {
		var result = eval( typed );

		if (! (result === undefined) )
			alert( result );
	}
	catch(ex) {
		alert( ex );
	}
}

function vimkeysScrollByLines(n) {
	if (n < 0) {
		n = 0-n
		for (var i = 0; i < n; i++) {
			goDoCommand('cmd_scrollLineUp')
		}
	}
	else {
		for (var i = 0; i < n; i++) {
			goDoCommand('cmd_scrollLineDown')
		}
	}

}

barsHere = true;
function toggleStuff() {
	// TODO: hide scroll bars too! ;(
	if (barsHere) {
		barsHere = false;
		document.getElementById('status-bar').setAttribute('collapsed', true);
		document.getElementById("nav-bar").setAttribute("collapsed",true);
		document.getElementById("scrollbar").setAttribute("collapsed",true);
	}
	else {
		barsHere = true;
		document.getElementById('status-bar').setAttribute('collapsed', false);
		document.getElementById("nav-bar").setAttribute("collapsed",false);
		document.getElementById("scrollbar").setAttribute("collapsed",false);
	}
}

function toggleTabs() {
	if (gBrowser.getStripVisibility()) {
		gBrowser.setStripVisibilityTo(false);
	}
	else {
		gBrowser.setStripVisibilityTo(true);
	}
}
